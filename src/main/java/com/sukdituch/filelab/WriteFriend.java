/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author focus
 */
public class WriteFriend {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("Worawit", 43, "0877899991");
            Friend friend2 = new Friend("Maguire", 26, "0888884888");
            Friend friend3 = new Friend("Anya", 8, "0811234567");
            //  นำ 3 Object ไปเก็บใน friends.dat
            //Create friend.dat
            File file = new File("friends.dat"); // บอกว่าจะมีไฟล์ไฟล์หนึ่งนะ
            //File not found exception หมายความว่า FileOutputStream จะไปเปิดไฟล์ที่ไม่มีอยู่จะทำการ throw exception ออกมา
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            //ทำให้สามารถเขียน Objectลงไปได้
            oos.writeObject(friend1);
            oos.writeObject(friend2);
            oos.writeObject(friend3);
            
            // เวลาเปิดให้ข้อมูลเข้าแล้ว ต้องทำการปิดด้วย
            oos.close();//เปิดทีหลังปิดก่อน
            fos.close();//เปิดก่อนปิดทีหลัง

        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
